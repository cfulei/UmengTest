package com.umeng.demo.bean;

public class TypeBean {

	private String name;
	private int logoID;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLogoID() {
		return logoID;
	}

	public void setLogoID(int logoID) {
		this.logoID = logoID;
	}

}
