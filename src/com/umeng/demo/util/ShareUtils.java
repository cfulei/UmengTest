package com.umeng.demo.util;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.umeng.demo.R;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.bean.UMShareMsg;
import com.umeng.socialize.controller.RequestType;
import com.umeng.socialize.controller.UMInfoAgent;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
import com.umeng.socialize.controller.listener.SocializeListeners.SocializeClientListener;
import com.umeng.socialize.controller.listener.SocializeListeners.UMAuthListener;
import com.umeng.socialize.exception.SocializeException;

public class ShareUtils {
	
	public static final String APP_ID = "wxd47f28a06fb24f9a";
	public static IWXAPI wxapi; 

	public static void ShareToSina(String content, final Context context) {
		UMSocialService controller = UMServiceFactory.getUMSocialService(
				context.getClass().getName(), RequestType.SOCIAL);
		if (!UMInfoAgent.isOauthed(context, SHARE_MEDIA.SINA)) {
			OA(controller, context, SHARE_MEDIA.SINA,new Handler());
			return;
		}
		// 构建分享内容
		UMShareMsg shareMsg = new UMShareMsg();
		// 设置分享文字
		shareMsg.text = content;
		// 设置分享图片
		// shareMsg.setMediaData(new UMRichMedia(data, MediaType.IMAGE));

		controller.postShare(context, SHARE_MEDIA.SINA, shareMsg,
				new SnsPostListener() {
					@Override
					public void onStart() {
						Toast.makeText(context, "开始分享.", Toast.LENGTH_SHORT)
								.show();
					}

					@Override
					public void onComplete(SHARE_MEDIA platform, int eCode,
							SocializeEntity entity) {
						if (eCode == 200) {
							Toast.makeText(context, "分享成功.", Toast.LENGTH_SHORT)
									.show();
						} else {
							String eMsg = "";
							if (eCode == -101)
								eMsg = "没有授权";

							Toast.makeText(context,
									"分享失败[" + eCode + "] " + eMsg,
									Toast.LENGTH_SHORT).show();
						}
					}
				});
	}
	
	public static void ShareToQZome(String content, final Context context) {
		UMSocialService controller = UMServiceFactory.getUMSocialService(
				context.getClass().getName(), RequestType.SOCIAL);
		
		
		if (!UMInfoAgent.isOauthed(context, SHARE_MEDIA.QZONE)) {
			OA(controller, context, SHARE_MEDIA.QZONE,new Handler());
			return;
		}
		// 构建分享内容
		UMShareMsg shareMsg = new UMShareMsg();
		// 设置分享文字
		shareMsg.text = content;
		// 设置分享图片
		// shareMsg.setMediaData(new UMRichMedia(data, MediaType.IMAGE));

		controller.postShare(context, SHARE_MEDIA.QZONE, shareMsg,
				new SnsPostListener() {
					@Override
					public void onStart() {
						Toast.makeText(context, "开始分享.", Toast.LENGTH_SHORT)
								.show();
					}

					@Override
					public void onComplete(SHARE_MEDIA platform, int eCode,
							SocializeEntity entity) {
						if (eCode == 200) {
							Toast.makeText(context, "分享成功.", Toast.LENGTH_SHORT)
									.show();
						} else {
							String eMsg = "";
							if (eCode == -101)
								eMsg = "没有授权";

							Toast.makeText(context,
									"分享失败[" + eCode + "] " + eMsg,
									Toast.LENGTH_SHORT).show();
						}
					}
				});
	}

	public static void ShareToTngxun(String content, final Context context) {

		UMSocialService controller = UMServiceFactory.getUMSocialService(
				context.getClass().getName(), RequestType.SOCIAL);
		if (!UMInfoAgent.isOauthed(context, SHARE_MEDIA.TENCENT)) {
			OA(controller, context, SHARE_MEDIA.TENCENT,new Handler());
			return;
		}
		// 构建分享内容
		UMShareMsg shareMsg = new UMShareMsg();
		// 设置分享文字
		shareMsg.text = content;
		// 设置分享图片
		// shareMsg.setMediaData(new UMRichMedia(data, MediaType.IMAGE));

		controller.postShare(context, SHARE_MEDIA.TENCENT, shareMsg,
				new SnsPostListener() {
					@Override
					public void onStart() {
						Toast.makeText(context, "开始分享.", Toast.LENGTH_SHORT)
								.show();
					}

					@Override
					public void onComplete(SHARE_MEDIA platform, int eCode,
							SocializeEntity entity) {
						if (eCode == 200) {
							Toast.makeText(context, "分享成功.", Toast.LENGTH_SHORT)
									.show();
						} else {
							String eMsg = "";
							if (eCode == -101)
								eMsg = "没有授权";

							Toast.makeText(context,
									"分享失败[" + eCode + "] " + eMsg,
									Toast.LENGTH_SHORT).show();
						}
					}
				});

	}

	/**
	 * 朋友圈
	 * @param content
	 * @param title
	 * @param context
	 * @param api
	 */
	public static void ShareToWeixin(final String content, final String title,
			final Context context, final IWXAPI api) {
		// String APP_ID = "wxfb7a618e26481015";
		// IWXAPI api;
		// api = WXAPIFactory.createWXAPI(TeWngMain.this, APP_ID, true);
		// api.registerApp(APP_ID);

//		WXTextObject textObject = new WXTextObject();
//		textObject.text = content;

		WXWebpageObject webObj = new WXWebpageObject(content);
		
		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = webObj;
		msg.title = title;
		msg.description = title;
		msg.setThumbImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
		

		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = System.currentTimeMillis() + "";
		req.message = msg;
		// req.scene = SendMessageToWX.Req.WXSceneSession;//会话
		req.scene = SendMessageToWX.Req.WXSceneTimeline;// 朋友圈
		api.sendReq(req);
		Log.e("ShareToWeixin", content);
	}
	
	/**
	 * 朋友
	 * @param content
	 * @param title
	 * @param context
	 * @param api
	 */
	public static void ShareToWeixin2(final String content, final String title,
			final Context context, final IWXAPI api) {
		// String APP_ID = "wxfb7a618e26481015";
		// IWXAPI api;
		// api = WXAPIFactory.createWXAPI(TeWngMain.this, APP_ID, true);
		// api.registerApp(APP_ID);

		WXWebpageObject webObj = new WXWebpageObject(content);
		
		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = webObj;
		msg.title = title;
		msg.description = title;
		msg.setThumbImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
		
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = System.currentTimeMillis() + "";
		req.message = msg;
		 req.scene = SendMessageToWX.Req.WXSceneSession;//会话
//		req.scene = SendMessageToWX.Req.WXSceneTimeline;// 朋友圈
		api.sendReq(req);
		Log.e("ShareToWeixin", content);
	}

	public static void OA(UMSocialService controller, final Context mContext,
			final SHARE_MEDIA shareMedia,final Handler handler) {

		controller.doOauthVerify(mContext, shareMedia, new UMAuthListener() {
			@Override
			public void onError(SocializeException e, SHARE_MEDIA platform) {
			}

			@Override
			public void onComplete(Bundle value, SHARE_MEDIA platform) {
				if (value != null && !TextUtils.isEmpty(value.getString("uid"))) {
					Toast.makeText(mContext, "授权成功.", Toast.LENGTH_SHORT)
							.show();
					if (shareMedia == SHARE_MEDIA.SINA) {
						handler.sendEmptyMessage(0);
					}else {
						handler.sendEmptyMessage(2);
					}
				} else {
					Toast.makeText(mContext, "授权失败", Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onCancel(SHARE_MEDIA arg0) {
			}

			@Override
			public void onStart(SHARE_MEDIA arg0) {
			}

		});

	}

	public static void unOA(UMSocialService controller, final Context mContext,
			final SHARE_MEDIA shareMedia,final Handler handler){
		controller.deleteOauth(mContext, shareMedia, new SocializeClientListener() {
			
			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onComplete(int arg0, SocializeEntity arg1) {
				// TODO Auto-generated method stub
				if (shareMedia == SHARE_MEDIA.SINA) {
					handler.sendEmptyMessage(1);
				}else {
					handler.sendEmptyMessage(3);
				}
			}
		});
	}

	public static boolean isOauthed(Context context, SHARE_MEDIA media) {
		if (!UMInfoAgent.isOauthed(context, media)) {
			return false;
		} else {
			return true;
		}
	}
	
	//用APP_ID 注册微信
	public static void regToWx(Context context){
		wxapi = WXAPIFactory.createWXAPI(context, APP_ID, true);
		wxapi.registerApp(APP_ID);
	}
	
}
