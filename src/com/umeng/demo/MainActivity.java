package com.umeng.demo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.umeng.demo.adapter.ListAdapter;
import com.umeng.demo.bean.TypeBean;
import com.umeng.demo.util.ShareUtils;

public class MainActivity extends Activity {

	private final Activity activity = this;
	private ListView listView;
	private ListAdapter adapter;
	private List<TypeBean> list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ShareUtils.regToWx(activity);
		listView = (ListView) findViewById(R.id.listView);
		list = getTypeList();
		adapter = new ListAdapter(activity, list);
		listView.setAdapter(adapter);
		setOnclicklistView();
		
	}

	private void setOnclicklistView(){
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				//Toast.makeText(activity, list.get(position).getName(), Toast.LENGTH_SHORT).show();
				switch (position) {
				case 0:
					//新浪微博分享
					ShareUtils.ShareToSina("友盟社交分享功能测试...", activity);
					break;
					
				case 1:
					//QQ空间分享
					ShareUtils.ShareToQZome("友盟社交分享功能测试...", activity);
					
					break;
					
				case 2:
					if(ShareUtils.wxapi == null){
						ShareUtils.regToWx(activity);
					}
					
					if(!ShareUtils.wxapi.isWXAppInstalled()){
						Toast.makeText(activity, "您尚未安装微信客户端...", Toast.LENGTH_SHORT).show();
						return;
					}else if(!ShareUtils.wxapi.isWXAppSupportAPI()){
						Toast.makeText(activity, "您安装微信客户端版本过低...", Toast.LENGTH_SHORT).show();
						return;
					}
					
					ShareUtils.ShareToWeixin2("测试微信客户端分享功能....", "测试", activity, ShareUtils.wxapi);
					break;
					
				case 3:
					
					//腾讯微博
					ShareUtils.ShareToTngxun("友盟社交分享功能测试...", activity);
					break;
					
				case 4:
					
					
					
					break;
					
				case 5:
					
					break;
					
				case 6:
					
					break;
					
				case 7:
					
					break;
					
				case 8:
					
					break;
					
				default:
					break;
				}
				
			}
		});
	}
	
	
	private List<TypeBean> getTypeList(){
		List<TypeBean> list = new ArrayList<TypeBean>();
		TypeBean type1 = new TypeBean();
		type1.setLogoID(R.drawable.umeng_socialize_sina_on);
		type1.setName("新浪微博");
		list.add(type1);
		
		TypeBean type2 = new TypeBean();
		type2.setLogoID(R.drawable.umeng_socialize_qzone_on);
		type2.setName("QQ空间");
		list.add(type2);
		
		TypeBean type4 = new TypeBean();
		type4.setLogoID(R.drawable.umeng_socialize_wechat);
		type4.setName("微信");
		list.add(type4);
		
		TypeBean type3 = new TypeBean();
		type3.setLogoID(R.drawable.umeng_socialize_tx_on);
		type3.setName("腾讯微博");
		list.add(type3);
		
		TypeBean type5 = new TypeBean();
		type5.setLogoID(R.drawable.umeng_socialize_qq_login);
		type5.setName("QQ");
		list.add(type5);
		
		TypeBean type7 = new TypeBean();
		type7.setLogoID(R.drawable.umeng_socialize_renren_on);
		type7.setName("人人网");
		list.add(type7);
		
		TypeBean type6 = new TypeBean();
		type6.setLogoID(R.drawable.umeng_socialize_douban_on);
		type6.setName("豆瓣网");
		list.add(type6);
		
		TypeBean type8 = new TypeBean();
		type8.setLogoID(R.drawable.umeng_socialize_sms);
		type8.setName("短信");
		list.add(type8);
		
		TypeBean type9 = new TypeBean();
		type9.setLogoID(R.drawable.umeng_socialize_gmail);
		type9.setName("邮箱");
		list.add(type9);
		return list;
	}

}
