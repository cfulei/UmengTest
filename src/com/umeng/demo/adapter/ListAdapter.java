package com.umeng.demo.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.umeng.demo.R;
import com.umeng.demo.bean.TypeBean;

public class ListAdapter extends BaseAdapter{

	private Context context;
	private List<TypeBean> list;
	private LayoutInflater inflater;
	
	public ListAdapter(Context mContext, List<TypeBean> list){
		this.context = mContext;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}

	@Override
	public View getView(int position, View covertView, ViewGroup parent) {
		if(list == null || list.size() <=0){
			return null;
		}
		TypeBean typeBean = list.get(position);
		covertView = inflater.inflate(R.layout.act_list_item, null);
		ImageView btn_logo = (ImageView) covertView.findViewById(R.id.logo);
		TextView txt_name = (TextView) covertView.findViewById(R.id.txt_name);
		
		btn_logo.setBackgroundResource(typeBean.getLogoID());
		txt_name.setText(typeBean.getName());
		
		return covertView;
	}

}
